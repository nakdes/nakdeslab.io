## Cyberpunk 2077 [GOG] [Proton]

- Proton 5.13-4 works but it may not start at all on some systems (for non-rolling-release distros)
- Arch users can use Proton-TKG which has less graphical bugs.

To fix crackling sound:
PULSE_LATENCY_MSEC=60<br><br>