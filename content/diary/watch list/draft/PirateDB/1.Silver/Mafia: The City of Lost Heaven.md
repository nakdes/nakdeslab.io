### Mafia: The City of Lost Heaven [GOG] [Wine]
- Game needs [widescreen fix](https://thirteenag.github.io/wfp#mafia) to make it work.
- Must first launch setup.exe to generate a config file before playing.
- Tested and working with Proton 5.21-GE-1.
