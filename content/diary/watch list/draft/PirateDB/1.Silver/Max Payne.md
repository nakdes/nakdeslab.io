## Max Payne [Wine]

- When the graphic option "Detail Textures" is set to High, there can sometimes be extremely faint bars on the screen that almost look like large horizontal scan lines. It is barely noticeable and does not affect gameplay. 

- An issue occurred mostly outdoors in "Part 2" of the game. This issue happens regardless if running the game in DX8 mode or with DXVK by using a DX8 to DX9 wrapper. Enabling or disabling the "ThirteenAG/WidescreenFix" does not resolve this. This issue can be completely avoided by setting the graphic option "Detail Textures" to medium or low.

- Limit the game to 60FPS or lower to prevent game breaking bugs with an uncapped frame rate.