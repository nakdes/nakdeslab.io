## Planetary Annihilation: TITANS [Linux Native]

- In order to play LAN game. Change your username with the optional: --username parameter. For example: "....TITANS/run.sh" --username Danger

- Different usernames are required in order to allow multiplayer. Next do NOT forget to select Open To: 'Public' (right top) when you hosted a local server, otherwise it will not be visible on the LAN network.