## Quake [Native]

- At first start the game will have a very low resolution of 640x480 and the font will be illegible. Open the options with Esc and navigate to the fourth menu item and enter with Enter. There are resolution options, and the bottom option is Apply.

- Make sure to toggle fullscreen.