## Total War Three Kingdoms [Goldberg] [Native]
- Goldberg's special specific client loader for three kingdoms is needed. Included in my torrent.
- Adding mods:

1. Add mod packs to the data directory
2. Register mods in the preferences.scripts.txt file as "mod modpackname.pack;" without the quotes
3. Change preferences.scripts.txt file to read-only (you can no longer save settings, but otherwise the game overwrites the changes after each time you close the game)<br>

- The mods will not show up on the launcher.