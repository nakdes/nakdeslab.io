
## Grand Theft Auto III [Wine]

- Remove constraints for grарhіс ѕеttіngѕ at commandline.txt with:
```sh
-norestrictions -nomemrestrict -percentvidmem 100 -novblank
```
- Silent patch improves compatibility with Wine.<br>

[Patch for GTA III](https://cookieplmonster.github.io/mods/gta-iii/)