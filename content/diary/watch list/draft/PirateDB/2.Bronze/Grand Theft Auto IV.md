## Grand Theft Auto IV - Complete Edition [Wne]

- Game does not work with proton, only wine. At least in my tests.

- To make the saves work, the games needs to think that the date is 23.5.2020<br>
     That can be done by just changing your date but it would break your internet connection.
     Another way to do it is to force wine to say this specific date.
     Here is wine compiled to make that happen [https://mega.nz/file/GKISjDoC#xx3GnApXHItm8QRdz4WaFKZ_0cT0Ysk2EjbgGgrtJFw](https://mega.nz/file/GKISjDoC#xx3GnApXHItm8QRdz4WaFKZ_0cT0Ysk2EjbgGgrtJFw) - compiled by toykiller


- Add these to commandline.txt to unlock max graphic settings.
-nomemrestrict -norestrictions -percentvidmem 100 -availablevidmem 6144 -noprecache -novblank -heapsize 2097152

- Recommended mods:
   FusionFix by El Dorado
   Graphical Bug Fixes v1.2 + EFLC PC Quality Texture Update v0.1 by nkjellman
   IV Fixes & Improvements v3.0 by Zolika1351
   Fixed water shaders for AMD-Radeon users
   Rancher Collision Model Fix by Phenom FX
   Karin Sultan Trunk Fix by jacell
   Colored Weapon HUD & Colored Radio HUD (Bonus)
   VisualIV
   Ultimate Textures v.2.0