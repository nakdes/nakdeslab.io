## Grand Theft Auto III/Vice City/San Andreas/IV [Wine]

- Remove constraints for grарhіс ѕеttіngѕ:
```sh
export EXE_KEY="-norestrictions"
export EXE_KEY1="-nomemrestrict"
export EXE_KEY2="-percentvidmem 100"
export EXE_KEY3="-novblank"
```
- Grand Theft Auto San Andreas has a spinning camera issue on Wine newer than 5.0 or Proton. This is fixed by the silent patch.