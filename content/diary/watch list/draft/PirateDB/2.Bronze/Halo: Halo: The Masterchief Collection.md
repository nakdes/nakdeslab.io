# Halo: The Masterchief Collection [Wine]

- If there is no sound in cutscenes, set Windows 7 on the prefix.

- A recent codex crack requires additional attention to work on Wine.

- The crack is inside winhttp.dll, wine runs it builtin by default and that breaks it.

- Run winetricks winhttp to fix this. - toykiller