## Mount & Blade II: Bannerlord [Wine]

- You need `dotnet48` installed

- Set brightness_calibrated = 1 on on `/Configs/engine_config.txt`
	
- If stuck in loading screen see [workaround](https://forums.taleworlds.com/index.php?threads/1-5-0-game-stuck-on-first-loading-screen.429316/)