## Persona 4 Golden
- Needs to be run in a 32-bit wine prefix
- Prefix needs 'wmp9 devenum d3dx11_43 quartz dxvk' (if it doesn't work with just these verbs also install lavfilters)
- Doesn't work properly outside of Proton 5.0.9

- The game will probably freeze and you won't be able to play it. So this is pretty much borked, but some people may be able to make it work, or just wait for wine/proton devs to fix it.