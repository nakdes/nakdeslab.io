## Resident Evil 3 [DENUVO-LESS] [Wine]

- This game needs `mf-install + mfplat.dll` from [link](https://github.com/z0z0z/mf-installcab)
- CODEX Steam emulator does not work, but you can replace it with [`Goldberg Steam Emu`](https://mr_goldberg.gitlab.io/goldberg_emulator/)
- You need to change these settings inside `re3_config.ini`:
```
TargetPlatform=DirectX11
PCDXver=DirectX11
```