## Sakuna of Rice and Ruin [Wine]

- Windows 10 mode will break audio. Use windows 7 mode instead in winecfg.
- Only works on wine-tkg from our tests.
- Game needs to be stopped from trying to find internet connection to stop lag spikes.