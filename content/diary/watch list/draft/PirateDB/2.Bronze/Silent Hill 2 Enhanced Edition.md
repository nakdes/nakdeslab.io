## Silent Hill 2 / Enhanced Edition [Wine]

[Enhancements](http://www.enhanced.townofsilenthill.com/SH2/) need to be applied in order to work better with wine.

When starting, it might also freeze with a white screen sometimes.
