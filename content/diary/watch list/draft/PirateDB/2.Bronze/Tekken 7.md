## Tekken 7 [EMPRESS] [Wine]

- The crack works fine on wine-staging, but it fails on wine-tkg.
- You will probably need to optimize the performance. Install a custom kernel like tkg and enable fsync.