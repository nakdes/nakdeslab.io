## The Dark Pictures Anthology: Man of Medan [Wine]

- Hooldum Steam emulator works fine on wine/wine-tkg, but the online-fix only works on Proton.
- You need to use PULSE_LATENCY_MSEC=60 to fix the audio.