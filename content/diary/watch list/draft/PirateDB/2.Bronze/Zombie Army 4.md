## Zombie Army 4 [EMPRESS] [Wine]

- Works with wine-tkg 5.6-5 and 6.0rc1 lutris build. Alternatively you can compile this https://github.com/codehungers/wine-4g-git which includes the patches to make the crack work on any wine version.

- Only works with v1 and v3 of the crack.

- Delete files in /drive_c/user/Application Data if it's crashing after it worked initially.